FROM python:latest

WORKDIR /home/ubuntu
COPY isPalindrome.py ./
EXPOSE 3000

ENTRYPOINT ["python3","/home/ubuntu/isPalindrome.py"]